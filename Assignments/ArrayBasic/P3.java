/*Given an array A[] of size n. The task is to find the largest element in it.
Example 1:
Input:
n = 5
A[] = {1, 8, 7, 56, 90}
Output: 90
Explanation:
The largest element of a given array is 90.
Example 2:
Input:
n = 7
A[] = {1, 2, 0, 3, 2, 4, 5}
Output: 5
Explanation:
The largest element of a given array is 5.
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(1)
Constraints:
1 <= n<= 10^3
0 <= A[i] <= 10^3
Arrays may contain duplicate elements.
*/

import java.util.*;
class Demo{

        public static void main(String[]args ){
                int arr[]=new int[]{3,3123123,2,1,56,12121,323};
                int N=5;
                int max=Integer.MIN_VALUE;
                


                for(int i=0;i<N;i++){
                        if(max<arr[i])
                                max=arr[i];

                }
                System.out.println("Largest= "+max);
                
        }
}
