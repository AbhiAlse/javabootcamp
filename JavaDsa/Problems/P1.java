//to find the squareroot of the number normal code

import java.io.*;
class Sqrt{
	 static int sqrtFun(int num){
		 int ans=0;
		 for(int i=1;i<=num;i++){
			 if(i*i==num){
				 ans=i;
				 return ans;
			 }
		 }
		 return ans ;
	 }
	 public static void main(String [] args)throws IOException{
		 
		 System.out.println("Enter any whole number");
		 BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		  
		 int num=Integer.parseInt(br.readLine());
		 int ans=sqrtFun(num);
		 System.out.println(ans);
	 }
}
		 
