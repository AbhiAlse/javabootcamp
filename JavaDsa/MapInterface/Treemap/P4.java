import java.util.*;

class Platform {

	String str=null;
	int foundYear=0;
	Platform (String str, int foundYear){
		this.str=str;
		this.foundYear=foundYear;
	}
	public String toString(){

		return "{"+str+":"+foundYear+"}" ;
	
	}
}
class Sortbyname implements Comparator<Platform>{
	
	public int compare(Platform obj1, Platform obj2){

		//return (((Platform)obj1).str).compareTo(((Platform)obj2).str);
		return obj1.str.compareTo(obj2.str);
	}
}
class Demo{

	public static void main(String []args){

		TreeMap tm=new TreeMap();
		tm.put(new Platform ("Youtube",2005),"Google");
		tm.put(new Platform ("Facebook",2004),"Meta");
		tm.put(new Platform ("Instagram",2007),"Meta");
		tm.put(new Platform ("ChatGpt",2022),"OpenAi");
	

		System.out.println(tm);

		Collections.sort(tm,new Sortbyname());
		
		System.out.println(tm);

	
	
	
	}
}
