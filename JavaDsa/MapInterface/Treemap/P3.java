import java.util.*;

class Platform implements Comparable{

	int foundYear=0;
	Platform ( int foundYear){
	
		this.foundYear=foundYear;
	}
	public String toString(){

		return "{" +foundYear+ ":" + "}" ;
	
	}
	public int compareTo(Object obj){
		return this.foundYear - ((Platform)obj).foundYear;
		
	}
}
class Demo{

	public static void main(String []args){

		TreeMap tm=new TreeMap();
		tm.put(new Platform (2005),"Google");
		tm.put(new Platform (2004),"Meta");
		tm.put(new Platform (2007),"Meta");
		tm.put(new Platform (2022),"OpenAi");
	

		System.out.println(tm);
	
	
	
	}
}
