import java.util.*;

class Demo{
	String str;
	Demo(String str){
		this.str=str;
	}
	public String toString(){
		return str;

	}
	public void finalize(){
		System.out.println("Notify");
	}
}
class Ihm{
	public static void main(String [] args){

		Demo obj1=new Demo("C2w");
		Demo obj2=new Demo("Binecaps");
		Demo obj3=new Demo("Incubator");
		IdentityHashMap hm=new IdentityHashMap();
		hm.put(obj1,"2016");
		hm.put(obj2,"2019");
		hm.put(obj3,"2023");

		obj1=null;
		obj2=null;
		System.gc();
		System.out.println(hm);
		System.out.println("In main");



	}
}
