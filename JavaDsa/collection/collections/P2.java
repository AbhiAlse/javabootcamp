import java.util.*;

class Employe{
	String name=null;
	float sal=0.0f;

	Employe(String name, float sal){

		this.name=name;
		this.sal=sal;
	}
	public String toString(){
		return "{"+name+": "+sal+"}";
	}

}
class sortbyname implements Comparator<Employe>{


	public int compare(Employe obj1,Employe obj2){

		return obj1.name .compareTo(obj2.name);
	}

}
class sortbysal implements Comparator<Employe>{


	public int compare(Employe obj1 , Employe obj2){

		return (int)(obj1.sal-obj2.sal);
	

	}

}

class listsortdemo{
	public static void main(String [] args){
		ArrayList al=new ArrayList();
		al.add(new Employe("Kanha",20000.0f));
		al.add(new Employe("Sumit",30000.0f));
		al.add(new Employe("Jay",25000.0f));
		al.add(new Employe("Kishan",27500.0f));
		al.add(new Employe("Mayur",13000.0f));

		
		System.out.println(al);

		Collections.sort(al,new sortbyname());

		System.out.println(al);
		Collections.sort(al,new sortbysal());
		System.out.println(al);


	}
}





