import java.util.*;

class Defence {
	String name=null;
	Defence(String str){

		this.name=str;
	}
	public String toString(){
		return name;
	}
}
class Demo{
	public static void main(String [] args){
		LinkedList obj=new LinkedList();
		obj.add(new Defence("Army"));
		obj.add(new Defence("Navy"));
		obj.add(new Defence("Air"));

		System.out.println(obj);
	}
}
