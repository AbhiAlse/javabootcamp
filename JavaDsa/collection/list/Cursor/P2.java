import java.util.*;

class CursorDemo{
	public static void main(String [] args){

		ArrayList al = new ArrayList();
		al.add("Abhi");
		al.add("Sumit");
		al.add("Nitin");
		al.add("Rahul");
		al.add("Shrikant");

		for(var x: al){
			System.out.println(al.getClass().getName());
		}


		Iterator itr= al.iterator();
		System.out.println(itr.getClass().getName());

		while( itr.hasNext()){
			System.out.println(itr.next());  //this method first moves forward and return the element where it is standing 
				
			if("Shrikant".equals(itr.next()))
				itr.remove();
		}	
		System.out.println(al);

		
	}
}
