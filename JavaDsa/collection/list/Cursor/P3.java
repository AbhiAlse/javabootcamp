import java.util.*;

class CursorDemo{
	public static void main(String [] args){

		ArrayList al = new ArrayList();
		al.add("Abhi");
		al.add("Sumit");
		al.add("Nitin");
		al.add("Rahul");
		al.add("Shrikant");

		


		ListIterator litr= al.listIterator();
		System.out.println(litr.getClass().getName());

		while( litr.hasNext()){
			System.out.println(litr.next());  //this method first moves forward and return the element where it is standing 
				
		}

		while(litr.hasPrevious()){      //gives bool value if there is element in prevuious box
			System.out.println(litr.previous()); //returns the ele and moves back
		}

		System.out.println(al);



	}
}
