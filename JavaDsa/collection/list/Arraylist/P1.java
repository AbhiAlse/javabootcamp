import java.util.*;
class ArraylistDemo extends ArrayList {
	public static void main(String [] args){
		ArraylistDemo obj=new ArraylistDemo();
		obj.add(10);
		obj.add(20);
		obj.add("Abhishek");
		obj.add("Alse");
		obj.add(30.5F);
		obj.add(20);

		System.out.println(obj); //printing an arraylist by its object

		System.out.println(obj.size()); //returns the integer value i.e = Size
		
		System.out.println(obj.contains(30));//checks weather it is present in ArrayList or not and return true,false
		System.out.println(obj.contains("Abhishek"));
	
		System.out.println(obj.indexOf("20"));//it returns the index value of element present in ArrayList and returns -1 if not present
		
		System.out.println(obj.lastIndexOf(20));//it returns the Lastindex value of element present in ArrayList and returns -1 if not present

		System.out.println(obj.get(2)); //it gives the object which is present at the index provided by that methed and throws ArrayIndexoutofException if beyond size

		System.out.println(obj.set(4,"Pune")); // here the 2 parameters 1st to which index toset it and next is the elment to set and returns the deleted element which was set istead of it

		System.out.println(obj);
		
		obj.add(2,"Core2web"); //here it adds the element at that index without deleting others
		
		System.out.println(obj);
		
		System.out.println(obj.remove(5)); //here it removes the given element at that index and returns the deleted elements
		System.out.println(obj);
		ArraylistDemo obj2=new ArraylistDemo();
		obj2.add("Prit");
		obj2.add("Ram");
		obj2.add("Kate");
		obj2.add("Onkar");

		System.out.println(obj2);

		obj.addAll(5,obj2); //it adds all the elements of other ArrayList at provided index without removing anyone
		//obj.addAll(obj2); //it adds all the elements of other ArrayList at he end 

		System.out.println(obj);
		obj.removeRange(3,5); //removes the element from 3 to 4
		System.out.println(obj);
		
		
		Object arr[]=obj.toArray();

		for(Object x:arr){
			System.out.print(" "+x);
		}
		
	

	}
}
