import java.util.*;

class Demo{
	int jerno=0;
	Demo(int n){
	
		this.jerno=n;
	
	}
	public String toString (){

		return Integer.toString(jerno);
		// The issue is related to the toString() method and how you're trying to return the value of jerno, which is an int. The toString() method should return a String, so you need to convert the int to a String explicitly. 
	}
}


class IterDemo{
	public static void main(String [] args){
		Vector v=new Vector(10);
		
		System.out.println(v.capacity());
		v.addElement(10);
		v.addElement(10.5);
		v.addElement(20.5f);
		v.addElement("Abhishek");
		v.addElement(new Demo(10));

		System.out.println(v);

	}
}

