import java.util.*;
class Movies implements Comparable{

	String name;
	float coll;

	Movies(String name, float coll){
		this.name=name;
		this.coll=coll;
	}
	
	public int compareTo(Object obj){
			
		return (name).compareTo(((Movies)obj).name);
		
	}
	public String toString(){
		return  name;
	}

}

class TreesetDemo{
	public static void main(String [] args){

		TreeSet ts =new TreeSet();
		ts.add(new Movies("Gadar", 150.0f));
		ts.add(new Movies("Sirat", 250.0f));
		ts.add(new Movies("Takatak", 70.0f));
		ts.add(new Movies("Omg", 160.0f));

		System.out.println(ts);

	}
}
