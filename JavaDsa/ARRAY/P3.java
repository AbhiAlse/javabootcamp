//given an array of size of N 
//return the count of pairs i j  with arr[i]+arr[j] =k
//arr[3,5,2,1,-3,7,8,15,6,13];
//n=10;
//k=10;
//note=i!=j
//o/p===6

class Demo{
	public static void main(String []a){

		int arr[]=new int[]{3,5,2,1,-3,7,8,15,6,13};
		int N=arr.length;
		int count =0;
		int k=10;
		for(int i=0;i<N;i++){
		
			for(int j=0;j<N;j++){
				if(i!=j){
					if(arr[i]+arr[j]==k){
						count++;
					}
				}
			}
		}
		
		System.out.println(count);

		}	
}
