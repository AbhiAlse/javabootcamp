//inplace prifixsum array
//construct an prefix sum array of the array size N in array itself

class Demo{
	public static void main(String [] args){
		int arr[]=new int[]{4,3,2};

		int N=arr.length;

		for(int i=1;i<N;i++){
			arr[i]=arr[i-1]+arr[i];
		}


		for(int x:arr)
			System.out.print(" "+x);

	}
}
