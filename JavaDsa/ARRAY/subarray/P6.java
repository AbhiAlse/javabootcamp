//print the max subarray sum

class Maxsubarray{
	public static void main(String [] args){
		int arr[] =new int []{-2,1,-3,4,-1,2,1,-5,4};
		int n=arr.length;
		int maxsum=Integer.MIN_VALUE;

		for(int i=0;i<n;i++){
			for(int j=i;j<n;j++){
				int sum=0;
				for(int k=i;k<=j;k++){
			
					sum=sum+arr[k];
				}
				if(maxsum<sum)
					maxsum=sum;
			}
		}
		System.out.println(maxsum);
	}
}


