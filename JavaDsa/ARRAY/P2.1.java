//given an integer array of size n
//count the no of elements having at least greater than itself
//arr=[2,5,1,4,8,0,8,1,3,8]
//optimize approach = by finding max

class Demo{
	public static void main(String [] ar){

		int arr[]=new int []{2,5,1,4,8,0,8,1,3,8};
		System.out.println("Count of no of elements having at least greater than itself");

		int max=Integer.MIN_VALUE;

		int count=0;
		for(int i=0;i<arr.length;i++){

			for(int j=0;j<arr.length;j++){

				if(max<arr[i]){
					max=arr[i];
					count++;
				}
			}
		}
		
		System.out.println(arr.length-count);

	}
}
