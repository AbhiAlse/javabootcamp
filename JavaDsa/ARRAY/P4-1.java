//Make array reverse
import java.util.*;
class Demo{
	public static void main(String [] args){

		Scanner sc=new Scanner(System.in);
		System.out.println("Enter array size");
		int N=sc.nextInt();
		int arr[]=new int[N];

		for(int i=0;i<N;i++){
			arr[i]=sc.nextInt();
		}	
		
		reverseArray(arr);
	}


	static void reverseArray(int arr[]){

		int start=0;
		int end=arr.length-1;

		while (start<end){

			int temp=arr[start];
			 arr[start]=arr[end];
			arr[end]=temp;
			start++;
			end--;

		}
		for(int x:arr)
			System.out.println(x);
	
	

	}
}

