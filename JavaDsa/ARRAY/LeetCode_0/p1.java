/* 1. Reverse Integer (Leetcode:- 7)

Given a signed 32-bit integer x, return x with its digits reversed. If reversing
x causes the value to go outside the signed 32-bit integer range [-231, 231
- 1], then return 0.
Assume the environment does not allow you to store 64-bit integers (signed
or unsigned).
Example 1:
Input: x = 123
Output: 321
Example 2:
Input: x = -123
Output: -321
Example 3:
Input: x = 120
Output: 21

Constraints:
-231 <= x <= 231 - 1
*/

import java.util.Scanner;

class Demo{
	public static void main(String [] args){
		System.out.println("Enter the Number");
		Scanner sc=new Scanner(System.in);
		int N=sc.nextInt();
		int temp=N;
		int x=10;
		int rev=0;
		while(N!=0){
			int rem=N%10;
			rev=rev*x+rem;
			N=N/10;
		}
		System.out.println("Reverse value is : "+rev);
	}
}





