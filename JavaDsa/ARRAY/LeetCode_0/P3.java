/* 3  Search Insert Position (LeetCode-35)

Given a sorted array of distinct integers and a target value, return the index
if the target is found. If not, return the index where it would be if it were
inserted in order.
You must write an algorithm with O(log n) runtime complexity.
Example 1:
Input: nums = [1,3,5,6], target = 5
Output: 2
Example 2:
Input: nums = [1,3,5,6], target = 2
Output: 1
Example 3:
Input: nums = [1,3,5,6], target = 7
Output: 4

Constraints:
1 <= nums.length <= 104
-104 <= nums[i] <= 104
nums contains distinct values sorted in ascending order.
-104 <= target <= 104
*/

import java.util.Scanner;
class Demo{
	public static void main(String [] args){
		Scanner sc=new Scanner (System.in);
		System.out.println("Enter array size");
		int N=sc.nextInt();
		int arr[]=new int[N];
		System.out.println("Enter array elements");
		for(int i=0;i<N;i++){
			arr[i]=sc.nextInt();
		}
	
		System.out.println("Enter Targeted value");
		int target=sc.nextInt();
		
		int start=0;
		int end=N;
		while(start<=end){
			int mid=(start+(end-start))/2;

			if(target==arr[mid]){
				System.out.println("Position is :"+mid);
				break;
			}
			else if(target<arr[mid])
				end=mid-1;
			else
				start=mid+1;

		}
		System.out.println(start);
		
	}
}
