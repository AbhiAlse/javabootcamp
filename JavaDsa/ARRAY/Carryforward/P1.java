//from given array build an array of leftmax of size N
//Build an array leftmax of size N
//leftmax of i contains the max for the index 0 to the index i
//arr[-3,6,2,4,5,2,8,-9,3,1]
//Leftmax=[-3,6,6,6,6,6,8,8,8,8]


import java.util.Scanner;
class Demo{
	public static void main(String []a){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter array Size");
		int N=sc.nextInt();
		int arr[]=new int[N];
		for(int x:arr)
			x=sc.nextInt();
		int leftmax[]=new int[N];
		int max=Integer.MIN_VALUE;
		
		arr[0]=leftmax[0];
		for(int i=1;i<N;i++){
			
			for(int j=1;j<=i;j++){

				if(max<arr[j]){
					max=arr[j];
					leftmax[i]=max;
				}	
			}
		}
		for(int i=0;i<N;i++)
			System.out.println(leftmax[i]);
	}
}
