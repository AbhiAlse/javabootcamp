//Prefix sum
//given an array of size n & q number of queries contains two parameters (s,e)
//arr[-3,6,2,4,,5,2,8,-9,3,1]
//N=10;
//q=3,               1,3=12
//                   2,7=12
//                   1,1=6
//optimization code


import java.util.*;
class Demo{
	public static void main(String [] args){
		int arr[]=new int[]{-3,6,2,4,5,2,8,-9,3,1};
		int Q=3;
		int parr[]=new int[arr.length];
		Scanner sc=new Scanner(System.in);
		parr[0]=arr[0];
		for(int i=1;i<arr.length;i++){
			
			parr[i]=parr[i-1]+arr[i];
		}
		for(int x:parr)
			System.out.println(x);

		for(int i=1;i<=Q;i++){
			int sum=0;
			System.out.println("Enter start end");
			int start=sc.nextInt();
			int end=sc.nextInt();
			sum=parr[end]-parr[start-1];
			System.out.println(sum);

		}
	}
}


