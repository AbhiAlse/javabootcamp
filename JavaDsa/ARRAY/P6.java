//Prefix sum
//given an array of size n & q number of queries contains two parameters (s,e)
//arr[-3,6,2,4,,5,2,8,-9,3,1]
//N=10;
//q=3,               1,3=12
//                   2,7=12
//                   1,1=6

import java.util.*;
class Demo{
	public static void main(String [] args){
		int arr[]=new int[]{-3,6,2,4,5,2,8,-9,3,1};
		int Q=3;
		
		Scanner sc=new Scanner(System.in);
		
		for(int i=1;i<=Q;i++){

			System.out.println("Enter start n end");

			int start=sc.nextInt();
			int end=sc.nextInt();

			int sum=0;
			for(int j=start;j<=end;j++){
				sum=sum+arr[j];
			}
			System.out.println(sum);

		}
	}
}




