/*
WAP to print the elements whose addition of digits is even.
Ex. 26 = 2 + 6 = 8 (8 is even so print 26)
Input :
Enter array : 1 2 3 5 15 16 14 28 17 29 123
Output: 2 15 28 17 123
*/
import java.io.*;
class P10{
	public static void main(String [] args)throws IOException{
		BufferedReader br=new BufferedReader (new InputStreamReader (System.in));
		System.out.println("Enter array size");
		
		int size=Integer.parseInt(br.readLine());
		int arr[]=new int[size];
		
		System.out.println("Enter arr1 elements");
		
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());	
		}
		System.out.println("the elements whose addition of digits is even");

			
		for(int i=0;i<arr.length;i++ ){
			int sum=0;
			int temp=arr[i];
			while(temp!=0){
				int rem=temp%10;
				sum=sum+rem;
				temp=temp/10;
			}
			if(sum%2==0){
				System.out.println(arr[i]);
			}
		}
		System.out.println("Original array is");
		for(int i=0;i<arr.length;i++){
			System.out.println(arr[i]);

		}
	}
}
		
