/*
 WAP to find the uncommon elements between two arrays.
Input :
Enter first array : 1 2 3 5
Enter Second array: 2 1 9 8
Output: Uncommon elements :
3
5
9
8
*/
import java.io.*;
class P8{
	public static void main(String [] args)throws IOException{
		BufferedReader br=new BufferedReader (new InputStreamReader (System.in));
		System.out.println("Enter array size");
		
		int size=Integer.parseInt(br.readLine());
		int arr[]=new int[size];
		
		System.out.println("Enter arr1 elements");
		
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());	
		}
	
		int arr2[]=new int [size];
		System.out.println("Enter arr2 elements");
		for(int i=0;i<arr2.length;i++){
			arr2[i]=Integer.parseInt(br.readLine());	
		}

		System.out.println("Uncommon elements are ");
		int temp;

		for(int i=0;i<size;i++){
			int flag=0;
			for(int j=0;j<size;j++){
				if(arr[i]==arr2[j]){
					flag=1;
				}
			}
				if(flag==0){
					System.out.println(arr[i]);
			}
		}
		for(int i=0;i<size;i++){
			int flag=0;
			for(int j=0;j<size;j++){
				if(arr2[i]==arr[j]){
					flag=1;
				}
			}
				if(flag==0){
					System.out.println(arr2[i]);
			
			}
		}
	}
}
