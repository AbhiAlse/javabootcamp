/*
 WAP to find the common elements between two arrays.
Input :
Enter first array : 1 2 3 5
Enter Second array: 2 1 9 8
Output: Common elements :
1
2
*/
import java.io.*;
class P7{
	public static void main(String [] args)throws IOException{
		BufferedReader br=new BufferedReader (new InputStreamReader (System.in));
		System.out.println("Enter array size");
		
		int size=Integer.parseInt(br.readLine());
		int arr[]=new int[size];
		
		System.out.println("Enter arr1 elements");
		
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());	
		}
	
		int arr2[]=new int [size];
		System.out.println("Enter arr2 elements");
		for(int i=0;i<arr2.length;i++){
			arr2[i]=Integer.parseInt(br.readLine());	
		}

		System.out.println("Common elements are ");

		for(int i=0;i<size;i++){
			for(int j=0;j<size;j++){
				if(arr[i]==arr2[j]){
					System.out.println(arr[i]);
				}
			}
		}

	}
}
		
