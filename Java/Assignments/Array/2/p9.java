/*
 Write a Java program to merge two given arrays.
 Array1 = [10, 20, 30, 40, 50]
 Array2 = [9, 18, 27, 36, 45]
Output :
Merged Array = [10, 20, 30, 40, 50, 9, 18, 27, 36, 45]
Hint: you can take 3rd array

*/
import java.io.*;
class P9{
	public static void main(String [] args)throws IOException{
		BufferedReader br=new BufferedReader (new InputStreamReader (System.in));
		System.out.println("Enter array size");
		
		int size=Integer.parseInt(br.readLine());
		int arr[]=new int[size];
		
		System.out.println("Enter arr1 elements");
		
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());	
		}
	
		int arr2[]=new int [size];
		System.out.println("Enter arr2 elements");
		for(int i=0;i<arr2.length;i++){
			arr2[i]=Integer.parseInt(br.readLine());	
		}

		int arr3[]=new int [size+size];
		int pos=0;

		for(int i=0;i<size;i++){
			arr3[pos]=arr[i];
			pos++;
		}
		for(int i=0;i<size;i++){
			arr3[pos]=arr2[i];
			pos++;
		}
		System.out.println("Mergerd array is :");

		for(int i=0;i<arr3.length;i++){
			System.out.println(arr3[i]);
		}



	}
}
		
