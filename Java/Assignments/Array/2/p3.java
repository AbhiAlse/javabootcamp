/*Write a Java program to find the sum of even and odd numbers in an array.
 * Display the sum value.
 * Input: 11 12 13 14 15
 * Output
 * Odd numbers sum = 39
 * Even numbers sum = 26
*/
import java.io.*;
class P3{
	public static void main(String [] args)throws IOException{
		BufferedReader br=new BufferedReader (new InputStreamReader (System.in));
		System.out.println("Enter array size");
		int size=Integer.parseInt(br.readLine());
		int arr[]=new int[size];
		System.out.println("Enter arr elements");
		int esum=0;
		int osum=0;
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
			if(arr[i]%2==0){
				esum=esum+arr[i];
			}else{
				osum=osum+arr[i];
		}
		}
		System.out.println("Sum of all  even elements are ="+esum);
		System.out.println("Sum of all odd elements are ="+osum);
	}
}
		
