/* Write a program, take 7 characters as an input , Print only vowels from the array
Input: a b c o d p e
Output : a o e
*/

import java.io.*;
class P4{
	public static void main(String [] args)throws IOException{
		BufferedReader br=new BufferedReader (new InputStreamReader (System.in));
		System.out.println("Enter Array size");
		int size=Integer.parseInt(br.readLine());
		System.out.println("Enter Array Elements");
		char arr[]=new char [size];

		for(int i=0;i<arr.length;i++){
			arr[i]=(char)(br.read());
			br.skip(1);
		}
		for(int i=0;i<arr.length;i++){
			if(arr[i]=='a'||arr[i]=='e'|| arr[i]=='i'|| arr[i]=='o'||arr[i]=='u'){
				System.out.println(arr[i]);
			}
		}
}
}
