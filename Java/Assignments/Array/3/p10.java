/*
 Write a program to print the second min element in the array
Input: Enter array elements: 255 2 1554 15 65 95 89
Output: 15

*/


import java.io.*;
class P10{

	public static void main(String [] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter size");
		
		int size=Integer.parseInt(br.readLine());
		
		int arr[]=new int[size];
		
		System.out.println("Enter elements");
	
		for(int i=0; i<arr.length; i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		int min1=arr[0];
		int min2=0;
		for(int i=0;i<arr.length;i++){
			if(min1>arr[i]){
				min2=min1;
				min1=arr[i];
				//min2=min1;
			}
		}
		System.out.println(min2);
	}
}
