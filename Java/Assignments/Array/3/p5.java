/*
   WAP to find a Perfect number from an array and return its index.
   Take size and elements from the user
Input: 10 25 252 496 564
Output: Perfect no 496 found at index: 3

*/
import java.io.*;
class P5{
	static void comp(int n,int ind){
		int sum=0;
		for(int i=1;i<n;i++){
			if(n%i==0){
				sum=sum+i;
			}
		}
		if(sum==n){
			System.out.println("Perfect no  "+n+" found at "+ind);
		}
	}

	public static void main(String [] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter size");
		int size=Integer.parseInt(br.readLine());
		int arr[]=new int [size];
		System.out.println("Enter elements");
		
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		for(int i=0;i<arr.length;i++){
			comp(arr[i], i);
		}
	}
}
				

