/*
 WAP to find an ArmStong number from an array and return its index.
 Take size and elements from the user
Input: 10 25 252 36 153 55 89
Output: Armstrong no 153 found at index: 4 
*/

import java.io.*;

class P8{

	static void strong(int n,int ind){
		int temp=n;
		int sum = 0;
		int count=0;
		while(temp!=0){
			int rem=temp%10;
			count++;
			temp=temp/10;
		}int temp2=n;
		while(temp2!=0){
			int rem=temp2%10;
			int product=1;
			for(int j=1;j<=count;j++){
				product=product*rem;
			}sum=sum+product;
			temp2=temp2/10;
		}
		if(n==sum){
			System.out.println("Armstrong no "+n+" found at "+ind);
		}	
	}
	public static void main(String [] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter size");
		
		int size=Integer.parseInt(br.readLine());
		 System.out.println(size);
		int arr[]=new int[size];
		
		System.out.println("Enter elements");
	
		for(int i=0; i<arr.length; i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
			
		for(int i=0;i<arr.length;i++){
			strong(arr[i], i);		  
		}
	}

}
