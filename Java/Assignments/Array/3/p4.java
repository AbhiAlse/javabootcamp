/*
WAP to find a prime number from an array and return its index.
Take size and elements from the user
Input: 10 25 36 566 34 53 50 100
Output: prime no 53 found at index: 5
*/
import java.io.*;
class P4{
	static void comp(int n,int ind){
		int count=0;
		for(int i=1;i<=n;i++){
			if(n%i==0){
				count++;
			}
		}
		if(count==2){

			System.out.println("prime no "+n+" found at "+ind);

	}
}
	public static void main(String [] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter size");
		int size=Integer.parseInt(br.readLine());
		int arr[]=new int [size];
		System.out.println("Enter elements");
		
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		for(int i=0;i<arr.length;i++){
			comp(arr[i], i);
		}
	}
}
				

