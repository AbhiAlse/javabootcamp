/*
 Write a program to print the second max element in the array
Input: Enter array elements: 2 255 2 1554 15 65
Output: 255
*/


import java.io.*;
class P9{

	public static void main(String [] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter size");
		
		int size=Integer.parseInt(br.readLine());
		 System.out.println(size);
		int arr[]=new int[size];
		
		System.out.println("Enter elements");
	
		for(int i=0; i<arr.length; i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		int max1=arr[0];
		int max2=0;
		for(int i=0;i<arr.length;i++){
			if(max1<arr[i]){
				max2=max1;
				max1=arr[i];
			}
		}
		System.out.println(max2);
	}
}
