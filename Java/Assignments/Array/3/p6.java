/*
 WAP to find a palindrome number from an array and return its index.
 Take size and elements from the user
Input: 10 25 252 36 564

Output: Palindrome no 252 found at index: 2

*/
import java.io.*;
class P6{

	static void comp(int n,int ind){
		int y=0;
		int temp=n;
		while(n!=0){
			int rem=n%10;
			y=10*y+rem;
			n=n/10;
		}
		if(temp==y){
			System.out.println("Palindrome no "+temp+" found at index "+ind);
		}
	}

	public static void main(String [] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter size");
		int size=Integer.parseInt(br.readLine());
		int arr[]=new int [size];
		System.out.println("Enter elements");
		
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		for(int i=0;i<arr.length;i++){
			comp(arr[i], i);
		}
	}
}
				

