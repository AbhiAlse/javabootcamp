/*
  WAP to find a Strong number from an array and return its index.
  Take size and elements from the user
Input: 10 25 252 36 564 145
Output: Strong no 145 found at index: 5
*/
import java.io.*;

class P7{

	static void strong(int n,int ind){
		int temp=n;
		int sum = 0;				
		while(temp!=0){

			int fact=1;
			int rem=temp%10;
			for(int i=1;i<=rem;i++){
				fact=fact*i;
				 
			}
			temp = temp/10;
			sum = sum + fact;
		}
		if(sum==n){
			System.out.println("Strong number "+n+" found at index :"+ind);
		}
	
	}
	public static void main(String [] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter size");
		
		int size=Integer.parseInt(br.readLine());
		 System.out.println(size);
		int arr[]=new int[size];
		
		System.out.println("Enter elements");
	
		for(int i=0; i<arr.length; i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
			
		for(int i=0;i<arr.length;i++){
			strong(arr[i], i);		  
		}
	}

}
