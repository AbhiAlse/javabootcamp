// check wheather the no is automorphic or not
class P12{
	public static void main(String [] args){
		int N=5;
		int sqrt=N*N;
		int count1=0;
		int count2=0;
		int temp1=N;
		int temp2=sqrt;
		int rem1,rem2;

		while(N!=0){
			count1++;
			rem1=N%10;
			rem2=temp2%10;
			if(rem1==rem2){
				count2++;
			}
			N=N/10;
			temp2=temp2/10;
		}
		if(count1==count2){
			System.out.println("Automorphic ");
		}else{
			System.out.println("Not a automorphic");
		}
	}
}


