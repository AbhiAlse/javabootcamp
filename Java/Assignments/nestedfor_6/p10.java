/*
   Write a program to take range as input from the user and print Armstrong numbers. ( Take a start and
   end number from a user )
Input: Enter start: 1
Enter end: 1650
Output: Armstrong numbers between 1 and 1650
1 2 3 4 5 6 7 8 9 153 370 371 407 1634
 */
import java.io.*;
class P10{
	static void arstrong(int no){
		int temp=no;
		int temp2=no;
		int sum=0;
		int count=0;
		while(temp!=0){
			count++;
			temp=temp/10;
		}

		while(temp2!=0){
			int rem=temp2%10;
			int product=1;
			for(int j=1;j<=count;j++){
				product=product*rem;
			}
			sum=sum+product;
			temp2=temp2/10;
		}

		if(sum==no){
			System.out.println(no);
		}
	}

	public static void main(String [] args)throws IOException{
		BufferedReader br=new BufferedReader (new InputStreamReader(System.in));

		System.out.println("Enter start number");
		int start=Integer.parseInt(br.readLine());
		System.out.println("Enter end number");
		int end=Integer.parseInt(br.readLine());
		System.out.println();
		for(int i=start;i<=end;i++){
			arstrong(i);
		}
	}
}

