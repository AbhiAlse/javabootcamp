/* Write a program to print the numbers divisible by 5 from 1 to 50 & the number is even also print the
 * count of even numbers.
 * Input: Enter a lower limit: 1
 * Enter upper limit: 50
 * Output: 10, 20, 30, 40, 50
 * Count = 5
 
 */
import java.io.*;
class P1{
	public static void main(String [] args)throws IOException{
		BufferedReader br=new BufferedReader (new InputStreamReader(System.in));

		System.out.println("Enter start number");
		int start=Integer.parseInt(br.readLine());
		System.out.println("Enter end number");
		int end=Integer.parseInt(br.readLine());
		int count=0;
		for(int i=start;i<=end;i++){
			if(i%5==0&&i%2==0){
				count++;
				System.out.print(i+", ");
			}
		}System.out.println();
		System.out.println("Count: "+count);
	}
}

