/*
   write a program to print a series of strong numbers from entered range. ( Take a start and end number
   from a user )
Input:-
Enter starting number: 1
Enter ending number: 150
Output:-
Output: strong numbers between 1 and 150
1 2 145
 */
import java.io.*;
class P9{
	static void strong(int no){
		int temp=no;
		int sum=0;
		while(temp!=0){
			int rem=temp%10;
			int fact=1;
			for(int i=1;i<=rem;i++){
				fact=fact*i;
			}
			sum=sum+fact;
			temp=temp/10;
		}
		if(sum==no){
			System.out.println(sum);
		}
	}
	public static void main(String [] args)throws IOException{
		BufferedReader br=new BufferedReader (new InputStreamReader(System.in));

		System.out.println("Enter start number");
		int start=Integer.parseInt(br.readLine());
		System.out.println("Enter end number");
		int end=Integer.parseInt(br.readLine());
		System.out.println();
		System.out.println("Palindrome no between "+start+" and "+end+" are");
		for(int i=start;i<=end;i++){
			
			strong(i);
		}
	}
}

