/*
 Write a program to take range as input from the user and print the reverse of all numbers. ( Take a
 start and end nu:wq
 mber from a user )
Input: Enter start: 100
Enter end: 200
Output: Palindrome numbers between 100 and 250
 */
import java.io.*;
class P7{
	public static void main(String [] args)throws IOException{
		BufferedReader br=new BufferedReader (new InputStreamReader(System.in));

		System.out.println("Enter start number");
		int start=Integer.parseInt(br.readLine());
		System.out.println("Enter end number");
		int end=Integer.parseInt(br.readLine());
		System.out.println();
		for(int i=start;i<=end;i++){
			int temp=i;
			int rev=0;
			int rem=0;
			while(temp!=0){
				rem=temp%10;
				rev=rev*10+rem;
				temp=temp/10;
			}
			System.out.println(rev);

		}
	}
}
