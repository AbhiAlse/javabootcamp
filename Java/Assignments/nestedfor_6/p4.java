/*
 * Write a program to take a range as input from the user and print perfect cubes between that range.
 * Input: Enter start: 1
 * Enter end: 100
 * Output: perfect cubes between 1 and 100
 */
import java.io.*;
class P4{
	public static void main(String [] args)throws IOException{
		BufferedReader br=new BufferedReader (new InputStreamReader(System.in));

		System.out.println("Enter start number");
		int start=Integer.parseInt(br.readLine());
		System.out.println("Enter end number");
		int end=Integer.parseInt(br.readLine());
		
		for(int i=start;i*i*i<=end;i++){
			System.out.println(i*i*i);
		}
	
	
	}
}

