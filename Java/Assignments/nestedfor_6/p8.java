/*
 Write a program to take range as input from the user and print Palindrome numbers. ( Take a start and
   end number from a user )
Input: Enter start: 100
Enter end: 250
Output: Palindrome numbers between 100 and 250
101 111 121 131 141 151 161 171 181 191 202 212 222
 */

import java.io.*;
class P8{
	static void palindrome(int no){
		int temp=no;
		int rev=0;
		while(temp!=0){
			int rem=temp%10;
			rev=rev*10+rem;
			temp=temp/10;
		}
		if(rev==no){
			System.out.println(rev);
		}
	}
	public static void main(String [] args)throws IOException{
		BufferedReader br=new BufferedReader (new InputStreamReader(System.in));

		System.out.println("Enter start number");
		int start=Integer.parseInt(br.readLine());
		System.out.println("Enter end number");
		int end=Integer.parseInt(br.readLine());
		System.out.println();
		System.out.println("Palindrome no between "+start+" and "+end+" are");
		for(int i=start;i<=end;i++){
			
			palindrome(i);
		}
	}
}

