/*
 /*                                                                                                                            Write a program to take 5 numbers as input from the user and print the count of digits in those                               numbers.                                                                                                                      Input: Enter 5 numbers :                                                                                                      5                                                                                                                             The digit count in 5 is 1                                                                                                     25                                                                                                                            The digit count in 25 is 2                                                                                                    225                                                                                                                           The digit count in 225 is 3                                                                                                   6548                                                                                                                          The digit count in 6548 is 4                                                                                                  852347                                                                                                                        The digit count in 852347 is 6                                                                                                */

import java.io.*;

class P6{
	static void count(int no){
		int countno=0;
		int temp=no;
		while(no!=0){
				countno++;
				no=no/10;
			}
			System.out.println("the digit count in "+temp+" is"+countno);
	}
	public static void main(String [] args)throws IOException{
		BufferedReader br=new BufferedReader (new InputStreamReader(System.in));
		System.out.println("Enter the size");
		int size=Integer.parseInt(br.readLine());
		for(int i=1;i<=size;i++){
			int no=Integer.parseInt(br.readLine());
			count(no);
		}
	}
}
