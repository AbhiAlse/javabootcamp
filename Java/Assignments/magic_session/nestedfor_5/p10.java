/*Q10
 * write a program to print a series of prime numbers from entered range. ( Take a start and end number
 * from a user )
 * Perform dry run at least from 10 to 20 ...
 * Input:-
 * Enter starting number: 10
 * Enter ending number: 100
 * Output:-
 * Series = 11 13 17 19 ..... 89 97

*/
import java.io.*;

class P10{
	static void prime(int no){
		int count=0;
		for(int i=1;i<=no;i++){
			if(no%i==0){
				count++;
			}
		}
			if(count==2){
				System.out.println(no);
			}
	
	}
	public static void main(String [] args)throws IOException{

		BufferedReader br=new BufferedReader (new InputStreamReader (System.in));
		
		System.out.println("Enter the start number");
		int start=Integer.parseInt(br.readLine());
		System.out.println("Enter the end number");
		int end=Integer.parseInt(br.readLine());
		System.out.println("Series = ");
		for(int i=start;i<=end;i++){
			prime(i);
		}
	}
}

	
