/* D4 C3 B2 A1
 * A1 B2 C3 D4
 * D4 C3 B2 A1
 * A1 B2 C3 D4
 */
import java.io.*;

class P1{
	public static void main(String [] args )throws IOException {
		BufferedReader br =new BufferedReader (new InputStreamReader(System.in));

		System.out.println("Enter the rows");
		int r=Integer.parseInt(br.readLine());

		int n=r;
		char ch=(char)(64+n);

		for(int i=1;i<=r;i++){
			for(int j=1;j<=r;j++){
				if(i%2!=0){
					System.out.print(ch+""+n+" ");
					ch--;
					n--;
				}else{
					System.out.print(ch+""+n+" ");
					ch++;
					n++;
				}
			}System.out.println();
			if(i%2!=0){
				ch++;
				n++;
			}else{
				ch--;
				n--;
			}
		}
	}
}
