/* program to print the following pattern
 Row =5;

 O
 14 13
 L K J
 9 8 7 6
 E D C B A

*/
import java.io.*;
class P7{
	public static void main(String [] args)throws IOException{
		BufferedReader br=new BufferedReader (new InputStreamReader(System.in));

		System.out.println("Enter rows");
		int r=Integer.parseInt(br.readLine());

		int no=r*(r+1)/2;
		char ch=(char)(64+no);

		for(int i=1;i<=r;i++){

			for(int j=1;j<=i;j++){
				if(i%2!=0){
					System.out.print(ch+" ");
					ch--;
					no--;
				}else{
					System.out.print(no+" ");
					ch--;
					no--;
				}
			}	System.out.println();
			
		}
	}
}


