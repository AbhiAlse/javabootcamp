/*Q9
Write a program to take a number as input and print the Addition of Factorials of each
digit from that number.
Input: 1234
Output: Addition of factorials of each digit from 1234 = 33*/
import java.io.*;

class P9{
	public static void main(String [] args)throws IOException{

		BufferedReader br=new BufferedReader (new InputStreamReader (System.in));
		
		System.out.println("Enter the number");
		int no=Integer.parseInt(br.readLine());
		int temp=no;
		int sum=0;
		int rem;
		while(no!=0){
			rem=no%10;
			int fact=1;
			for(int i=1;i<=rem;i++){
				fact=fact*i;
			}
			sum=sum+fact;
			no=no/10;
		}
		System.out.println("Addition of factors of each digit are "+sum);
	}
}

	
