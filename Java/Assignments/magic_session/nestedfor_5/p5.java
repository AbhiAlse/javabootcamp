/* 0
 * 1 1
 * 2 3 5
 * 8 13 21 34
 */
import java.io.*;
class P5{
	public static void main(String [] args)throws IOException{
		BufferedReader br=new BufferedReader (new InputStreamReader(System.in));
		System.out.println("Enter the rows");
		int r=Integer.parseInt(br.readLine());
		int prev=0;
		int cur=1;
		int sum=0;
		for(int i=1;i<=r;i++){
			for(int j=1;j<=i;j++){
				System.out.print(sum+" ");
				prev=cur;
				cur=sum;
				sum=prev+cur;
			}
	System.out.println();
	}
}
}
