//sum of array  elements
import java.io.*;

class P7{
	public static void main(String [] args)throws IOException{
	BufferedReader br=new BufferedReader (new InputStreamReader(System.in));

	System.out.println("Enter the Array size");
	int Size=Integer.parseInt(br.readLine());
	int arr[]=new int [Size];
	int sum=0;
	for(int i=0;i<arr.length;i++){
		arr[i]=Integer.parseInt(br.readLine());
		sum=sum+arr[i];
	}
	System.out.println("Sum="+sum);
	}
}
