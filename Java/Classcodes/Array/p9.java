//count even odd elements in array
import java.io.*;
class P9{
	public static void main(String [] args)throws IOException{
		
		BufferedReader br=new BufferedReader (new InputStreamReader (System.in));
		System.out.println("Enter the array size");
		int size=Integer.parseInt(br.readLine());
		int arr[]=new int [size];
		int Evencount=0;
		int Oddcount=0;
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
			if(arr[i]%2==0){
				Evencount++;
			}else{
				Oddcount++;
			}
		}
		System.out.println("Evem count in array is ="+Evencount );
		System.out.println("Odd count in array is ="+Oddcount );
	}
}
