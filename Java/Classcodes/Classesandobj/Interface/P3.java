//multiple inheritence in java is solved using interface

interface Demo1{
	void fun();

}
interface Demo2{

	void fun();
}
class Demochild implements Demo1,Demo2{
	public void fun(){
		System.out.println("In Demochild");
	}
}
class Client{
	public static void main(String [] args){
		Demo1 obj=new Demochild();
		obj.fun();
	}
}
