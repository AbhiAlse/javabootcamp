interface Demo1{
	default void fun(){
		System.out.println("In demo1 fun");
	}

}
interface Demo2 extends Demo1{
	default void fun(){
		System.out.println("In Demo2 fun");
 	}
}

class Child implements Demo2{
          						//default void fun comes in child class and can be override

}
class Client{
	public static void main(String [] args){
		Demo obj=new Child();
		obj.fun();
	}
}

