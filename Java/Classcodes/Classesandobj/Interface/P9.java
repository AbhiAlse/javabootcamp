interface Demo{
	static void fun(){
		System.out.println("In demo fun");
	}
}

class  Demo2 implements Demo{

}
class Client {
	public static void main(String [] args){
		Client obj=new Client();
		Demo.fun();
		Demo2 obj2=new Demo2();
		obj2.fun(); //error static method of interface dont come 
	}
}
