interface Demo1{
	static  void fun(){           //static methods dont get invole in inheritence 

		System.out.println("In fun");
	}
	default void gun(){

		System.out.println("In gun");
	}
}
interface Demo2 extends Demo1{
		

}
class Demo3 implements Demo2{
		
	public static void main(String [] args){
		fun();
		gun();
	}
}
