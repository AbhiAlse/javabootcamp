interface Demo1{
	void gun();
	default void fun(){
		System.out.println("In Demo1 fun");
	}
}
class child implements Demo1{

	public void gun(){

		System.out.println("In child gun");
	
	}
	
}
class Client {
	public static void main(String[] args ){
		child obj=new child();
		obj.gun();
		obj.fun();
	}
}


