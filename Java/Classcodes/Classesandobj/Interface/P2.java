interface Demo{
	void fun();
	void gun();
}
abstract class Demochild implements Demo{
	public void gun(){
		System.out.println("In gun");
	}
}
class Demochild2 extends Demochild{
	public void fun(){
		System.out.println("In fun");
	}
}
class Client{
	public static void main(String [] args){
		 Demochild obj =new Demochild2(); //we can give the interface of Interface also
		obj.fun();
		obj.gun();
	}
}


