import java.io.*;

class Demo{
	public static void main(String [] args)throws IOException{
		
		BufferedReader br=new BufferedReader (new InputStreamReader(System.in));
		String str=br.readLine();
		System.out.println(str);

		br.close();
		String str1=null;
		try{	

			 str1=br.readLine();

		}catch(IOException e){
			System.out.println(e);
		}
		System.out.println(str1);

	}
}


