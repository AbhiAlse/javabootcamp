import java.io.*;

class Demo{

	void m1()throws IOException{
		
		System.out.println("In m1");
		BufferedReader br =new BufferedReader (new InputStreamReader(System.in));
		String str=br.readLine();
		int x=Integer.parseInt(br.readLine());             //if we give string here it will give numberformat exception
		System.out.println(str);
		System.out.println("end m1");
	}
	public static void main(String [] args) throws IOException {

		System.out.println("Start main");
		Demo obj=new Demo();
		obj.m1();
		System.out.println("end main");


	}
}
