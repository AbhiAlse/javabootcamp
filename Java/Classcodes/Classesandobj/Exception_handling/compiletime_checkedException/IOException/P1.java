import java.io.*;

class Demo{
	public static void main(String [] args){  //throws IOException {

		BufferedReader br =new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter name");
		String str=null;
		try{                       // here we have to wright the code that is throwing the IOException  
			str=br.readLine();  
		}
		catch(IOException obj){      // in catch=we have tp Catch that exception
			System.out.println("Exp caught");
		}
		System.out.println(str);
	}
}

