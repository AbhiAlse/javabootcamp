import java.util.Scanner;

class DataOverFlowException extends RuntimeException {
	DataOverFlowException(String str){
		super(str);
	}
}
class DataUnderFlowException extends RuntimeException {
	DataUnderFlowException(String str){
		super(str);
	}
}
class ArrayDemo{
	public static void main(String[]a){
		int arr[]=new int [5];
		
		Scanner sc=new Scanner(System.in);

		System.out.println("Enter integer  value");
		System.out.println("Please enter between 0 and 100");
		
		for(int i=0;i<arr.length;i++){
			int data=sc.nextInt ();
			if(data<0){
				throw new DataUnderFlowException("Motha number de ");
			}
			if(data>100){
				throw new DataOverFlowException("lahan number de ");
			}
			arr[i]=data;
		}
		for(int i=0;i<arr.length;i++){
			System.out.println(arr[i]);
		}
	}
}



