import java.util.concurrent.*;

class MyThread implements Runnable{

	int num;
	MyThread (int num){
		this.num=num;
	}
	public void run(){
		
		System.out.println(Thread.currentThread()+ " Start Thread: "+num);
		dailyTask();
		System.out.println(Thread.currentThread()+ " End Thread: "+num);
	}
	void dailyTask(){
		try{
			Thread.sleep(10000);
		}
		catch(InterruptedException ie){

		}
	}
}
class ThreadpoolDemo{
	public static void main(String []args){
		ThreadPoolExecutor tpe=(ThreadPoolExecutor)(Executors.newCachedThreadPool());
		ThreadPoolExecutor tpe2=(ThreadPoolExecutor)(Executors.newCachedThreadPool());
		for(int i=1;i<=10;i++){
			MyThread obj =new MyThread (i);
			tpe.execute(obj);
			tpe2.execute(obj);
		}
		tpe.shutdown();
		tpe2.shutdown();
	}
}

