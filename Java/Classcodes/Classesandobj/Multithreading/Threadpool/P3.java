import java.util.concurrent.*;

class MyThread implements Runnable{
	int num;
	MyThread(int num){
		this.num=num;
	}
	public void run(){
		System.out.println(Thread.currentThread()+" Start Thread :"+num);
		dailyTask();
		System.out.println(Thread.currentThread()+" End Thread :"+num);
	}
	void dailyTask(){
		try{
			Thread.sleep(5000);
		}catch(InterruptedException  ie){

		}
	}
}
class ThreadGroupDemo{
	public static void main(String [] args){
		ExecutorService ser=Executors.newSingleThreadExecutor(); // here single  thread is only creadted so task executio is very

		for(int i=1;i<10;i++){
			MyThread obj=new MyThread(i);
			ser.execute(obj);
		}
		ser.shutdown();
	}
}
