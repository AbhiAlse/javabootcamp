class MyThread extends Thread{
	MyThread(ThreadGroup tg,String str){
		super(tg,str);
	}
	public void run(){
		System.out.println(Thread.currentThread());
	}
}
class ThreadGroupDemo{
	public static void main(String [] args){
		ThreadGroup ptg=new ThreadGroup("Core2Web");

		MyThread obj1=new MyThread (ptg,"C");
		MyThread obj2=new MyThread (ptg,"Java");
		MyThread obj3=new MyThread (ptg,"CPP");
		MyThread obj4=new MyThread (ptg,"OS");

		obj1.start();
		obj2.start();
		obj3.start();
		obj4.start();
	}
}

