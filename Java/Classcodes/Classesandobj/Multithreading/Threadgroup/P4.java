class MyThread extends Thread {

	MyThread(ThreadGroup tg, String str){
		super(tg,str);
	}
	public void run(){
		System.out.println(Thread.currentThread());
		try{
			Thread.sleep(3000);
		}catch(InterruptedException ie){
			
			System.out.println(ie.toString());
		}
	}
}
class ThreadGroupDemo{
	public static void main(String [] args)throws InterruptedException{
		ThreadGroup tg1=new ThreadGroup("Icc");
		
		MyThread t1=new MyThread(tg1,"BCCI");
		MyThread t2=new MyThread(tg1,"PCB");
		MyThread t3=new MyThread(tg1,"ACB");
		t1.start();
		t2.start();
		t3.start();
		
		ThreadGroup ctg1=new ThreadGroup(tg1,"India");
		
		MyThread t4=new MyThread(ctg1,"IPL");
		MyThread t5=new MyThread(ctg1,"TPL");
		MyThread t6=new MyThread(ctg1,"PPL");
		t4.start();
		t5.start();
		t6.start();
		
		Thread.sleep(2000);
		
		System.out.println(tg1.activeCount());
		System.out.println(tg1.activeGroupCount());
		
		
	}
}


