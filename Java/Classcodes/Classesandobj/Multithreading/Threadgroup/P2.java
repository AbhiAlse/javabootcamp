//creating multiple Threads by giving name

class MyThread extends Thread{
	MyThread(String str){			
		super(str);
	}
	MyThread(){			
		super();
	}
	
	
		public void run(){
			//System.out.println(Thread.currentThread().getThreadGroup());
			System.out.println(Thread.currentThread());
		}
}
class ThreadGroupDemo{
	public static void main(String [] args){
		MyThread obj1=new MyThread("Pqr");
		MyThread obj2=new MyThread("MNO");
		MyThread obj3=new MyThread();       //without Passing string it will count from 0
		MyThread obj4=new MyThread();       
		obj4.setName("XYZ");
		obj1.start();
		obj2.start();
		obj3.start();
		obj4.start();
		
		System.out.println(Thread.currentThread().getThreadGroup());
	}
}
