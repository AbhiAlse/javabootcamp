//using Runnable

class MyThread implements Runnable{

	public void run(){
		System.out.println(Thread.currentThread());
		try{
			Thread.sleep(3000);
		}
		catch(InterruptedException ie){
			
			System.out.println(ie.toString());
		}
	}
}
class ThreadGroupDemo{
	public static void main(String [] args){
		ThreadGroup tg1=new ThreadGroup("Icc");
	
		MyThread obj1=new MyThread();
		MyThread obj2=new MyThread();
		MyThread obj3=new MyThread();

		Thread t1=new Thread (tg1,obj1,"BCCI");
		Thread t2=new Thread (tg1,obj2,"PCB");
		Thread t3=new Thread (tg1,obj3,"ACB");
		t1.start();
		t2.start();
		t3.start();
		
		ThreadGroup ctg1=new ThreadGroup(tg1,"India");
		
		MyThread obj4=new MyThread();
		MyThread obj5=new MyThread();
		MyThread obj6=new MyThread();
		
		Thread t4=new Thread(ctg1,obj4,"PPL");
		Thread t5=new Thread (ctg1,obj5,"PCB");
		Thread t6=new Thread (ctg1,obj6,"ACB");
		
		t4.start();
		t5.start();
		t6.start();
		
		System.out.println(tg1.activeCount());
		System.out.println(tg1.activeGroupCount());
		
		
	}
}


