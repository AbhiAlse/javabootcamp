//Priority in Threads 
class MyThread extends Thread{
	public void run(){
		Thread t=Thread.currentThread();
		System.out.println(t.getPriority());
	}
}
class ThreadDemo{
	public static void main(String [] a){
		Thread t=Thread.currentThread();
		System.out.println(t.getPriority());  //5

		MyThread obj=new MyThread();
		obj.start();                         //

		t.setPriority(7);
		System.out.println(t.getPriority());

		MyThread obj2=new MyThread();
		obj2.start();
		
		

	}
}
