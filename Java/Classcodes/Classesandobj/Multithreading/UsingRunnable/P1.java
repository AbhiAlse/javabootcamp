class MyThread implements Runnable{
	public void run(){
		System.out.println(Thread.currentThread());
		System.out.println("In run");
		

	}
}

class Demo{
	public static void main(String [] args){
		MyThread obj=new MyThread();
		//obj.start();    we cannot use start method because the thread is not yet made by above line we have to bring Thread Class in picture 
		Thread obj1=new Thread(obj);
		Thread t=Thread.currentThread();
		obj1.start();
		System.out.println("In Main");
		System.out.println(t);

	}
}
