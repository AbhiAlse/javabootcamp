//Deadlock Scenario overcomming By passing parameter to join
class MyThread extends Thread{
	static Thread mThread=null;
	public void run(){
		try{
			mThread.join(2000);            //nahe pehle aap so it goes to mai Thread 
		}catch(InterruptedException obj){

		}
		for(int i=0;i<10;i++){
			System.out.println("In run");
		}
	}
}
class ThreadDemo {
	public static void main(String [] args)throws InterruptedException{
		MyThread.mThread=Thread.currentThread();
		MyThread obj=new MyThread();
		obj.start();
		obj.join();           //pehle aap so it goes to Thread-0
		for(int i=0;i<10;i++){
			System.out.println("In main");
		}
	}
}
