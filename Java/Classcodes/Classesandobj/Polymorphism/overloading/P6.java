class Demo{
	void fun(String str1){
		System.out.println("String");
	}

	void fun(StringBuffer str1){
		System.out.println("StringBuffer");
	}
}
class Client{
	public static void main(String [] args){
		Demo obj=new Demo();
		obj.fun("Abhishek");
		obj.fun(new StringBuffer("Abhi"));
		obj.fun(null);                  // here ambiguous error becoz he dont know where to go it works with both fun

	}
}
