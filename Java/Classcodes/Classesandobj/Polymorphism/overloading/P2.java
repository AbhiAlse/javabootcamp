class Demo{
	void fun(){
		System.out.println("In fun");
	}
	void fun(int x){

		System.out.println(x);
	}
	
	public static void main(String [] args){

		//Object obj= new Demo();		
		Demo obj= new Demo();		
		obj.fun();
		obj.fun(10);
		obj.main();
		obj.main(10);

	}
	void main(){                                 //here it is possible to owvrload the main methdo

		System.out.println("In main Method");
	}
	void main(int x){

		System.out.println("In para main Method");
	}
}
