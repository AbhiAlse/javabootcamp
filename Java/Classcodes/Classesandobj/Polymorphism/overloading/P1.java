// one name many form in same class
class Demo {
	Demo(){
		System.out.println("In Demo cons");
	}
	Demo(int x){
		System.out.println(x);
	}
	
	public static void main(String [] args){
		Demo obj=new Demo();
		Demo obj1=new Demo(10);
	}
}
