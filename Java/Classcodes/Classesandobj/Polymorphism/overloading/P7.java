class Demo{
	void fun(Object obj){
		System.out.println("Object");
	}
	void fun(String str){
		System.out.println("String");
	}
}
class Client{
	public static void main(String[] args){
		Demo obj=new Demo();
	//	Object str="Abhi";           // It works with thew refernce of Parent class    
	//	Object obj1=null;            // Like this it works
		obj.fun("Abhi");
		obj.fun(new StringBuffer("Abhishek Alse"));
		obj.fun(null);                         // It works with both but gives to parent class 

	}
}

