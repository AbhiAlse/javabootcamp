class Parent{
	Parent(){
		System.out.println("Parent cons");
	}
	private void fun(){
		System.out.println("In fun");
	}
}
class Child extends Parent{
	Child(){
		System.out.println("Child cons");
	}
	void gun(){

		System.out.println("In gun");
	}
}
class Client{
	public static void main(String [] args){
					
		Child obj=new Child();
		obj.fun();
		obj.gun();
		Parent obj2=new Parent();
		obj2.fun();
		//obj2.gun();
	}
}
