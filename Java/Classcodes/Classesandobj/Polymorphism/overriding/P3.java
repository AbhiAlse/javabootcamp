class Parent{
	Parent(){
		System.out.println("In parent constructor");
	}
	void Property(){
		System.out.println("Home Car Gold");
	}
	void Marry(){
		System.out.println("Depika Padukone");
	}
}
class Child extends Parent{
	Child(){
		System.out.println("In Child constructor");
	}
	
	void Marry(){                            //this method is override
		System.out.println("Alia Bhatt");
	}
}
class Client{
	public static void main(String [] args){
		Child obj=new Child();
		obj.Property();
		obj.Marry();	
	}
}
