class Parent{
		
	private void fun(){
		System.out.println("In parent fun");
	}
}
class Child extends Parent {
	 void fun(){  // fun in Parent class is private
		System.out.println("In parent fun");
	}
}
class Client{
	public static void main(String [] args){
		
			Parent obj =new Child();
			obj.fun();
		
	}
}
