class Parent{
	Parent(){
		System.out.println("Parent cons");
	}
	void fun(){
		System.out.println("In Parent fun");
	}
}
class Child extends Parent{
	Child(){
		System.out.println("Child cons");
	}
	void fun(int x){
		System.out.println("In child fun");
	}
}
class Client{
	public static void main(String [] args){
	Parent obj =new Child();                  //Parent reference  =  Child Obj
	//While Compile time compiler look at only right side it looks the fun method is there is Reference Given class or not then in run time it gives peiority to child class
	obj.fun();
	}
}





