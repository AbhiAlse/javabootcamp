abstract class Parent{
	 void marry(){
		 System.out.println("I will marry with Janny");
	 }
}
class Child extends Parent{
	void marry(){
		System.out.println("I will marry with John");
	}
}
class Client{
	public static void main(String [] args){
		
		Parent obj=new Child();
		obj.marry();

	}
}

