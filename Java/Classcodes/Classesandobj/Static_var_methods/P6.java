class Demo{
	static {
		System.out.println("In static 1 ");
	}
	Demo(){

		System.out.println("In Comstructor Demo");
	}
	{

		System.out.println("In Instance Demo");
	}
	static void fun(){

		System.out.println("In Fun");
	}

}
class Client {
	public static void main(String [] args){
		Demo obj=new Demo();
		obj.fun();

		System.out.println("In Main ");
	}
	
	static {
		System.out.println("In static 2 ");
	}
	Client(){

		System.out.println("In Comstructor Client");
	}
	{

		System.out.println("In Instance client");
	}
}
