class Demo{
	static int x=10;
	static int y=20;

	static void fun(){

		System.out.println(x);
		System.out.println(y);
	}
}
class Client{
	public static void main(String []a){
		System.out.println(Demo.x);
		System.out.println(Demo.y);
		Demo.fun();
		Demo.x=100;
		System.out.println(Demo.x);
		Demo.fun();
	}
}

