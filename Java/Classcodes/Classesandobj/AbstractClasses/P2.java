//Real time Eg on Abstract classes

abstract class Defence{
	void Aim(){
		System.out.println("To protect our country");
	}
	abstract abhivoid Weapons();
	abstract void DressCode();
		
}
class Army extends Defence{
	void Weapons(){
		System.out.println("We have our own Fighting Plan with good anumations");
	}
	void DressCode(){
		System.out.println("We have our own Dress code different from other's");
	}
}

class Client{
	public static void main(String [] args){
			
		Defence obj=new Army();
		obj.Aim();
		obj.Weapons();
		obj.DressCode();
	}
}         
