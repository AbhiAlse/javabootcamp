class Player{
	private int jno=0;
	private String name="null";
	Player(int jno,String name){
		this.jno=jno;
		this.name=name;
	}
	void info(){
		System.out.println(jno);
		System.out.println(name);
	}
}
class Client{
	public static void main(String [] args){
		Player obj=new Player(18,"Virat");   //player(obj,18,"Virat")
		obj.info();
		Player obj2=new Player(07,"Dhoni");
		obj2.info();

	}
}

