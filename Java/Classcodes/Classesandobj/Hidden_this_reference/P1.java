class Demo{
	int x=10;
	Demo(){
		this();
		System.out.println("In no args Const");
	}
	Demo(int x){
		this();
		System.out.println("In Para  Const");
	}
	public static void main(String [] args){
		Demo obj1=new Demo();
		Demo obj2=new Demo(10);
	}
}

