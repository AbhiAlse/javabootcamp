class Demo{
	int x=10;
	private int y=20;
	static int z=30;

	void disp(){
		System.out.println(x);
		System.out.println(y);
		System.out.println(z);
	}
}
class Client{
	public static void main(String []a){

		Demo obj=new Demo();
		Demo obj2=new Demo();

		obj.disp();
		obj.x=100;
		obj2.z=300;
		obj.disp();
		obj2.disp();

	}
}



