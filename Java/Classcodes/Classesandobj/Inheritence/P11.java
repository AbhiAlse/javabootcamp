class Parent {
	int x=10;
	static int y=20;
	Parent (){
		System.out.println("In parent cons");
	}
	void m1(){
		System.out.println("In parent m1");
	}
	static void m2(){

		System.out.println("In parent m2");
	}
}
class child extends Parent{
	int a=50;
	static int b=60;
	child(){
		System.out.println("In child constructor");
	}
	void m1(){
		System.out.println("In child m1");
	}
	static void m3(){
		System.out.println("In child m3");
	}
}
class Client{
	public static void main(String [] args){
			
		Parent obj=new Parent();
		obj.m1();
		obj.m2();
		
		child obj2=new child();
		obj2.m1();
		obj2.m2();
		obj2.m3();
		
		Parent obj3=new child();
		obj3.m1();
		obj3.m2();
	//	obj3.m3();   error=becoz it is not present in parent class as reference is of parent class
	}
}
