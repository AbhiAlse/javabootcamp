class Parent{
	Parent(){
		System.out.println("In Parent cons");
		System.out.println(this);
	}
	void parentprop(){
		System.out.println("flat gold");
	}
}
class Child extends Parent{
	Child(){
		System.out.println("In child Cons");
		System.out.println(this);
	}
}

class Childson extends Child{
	Childson(){
		System.out.println("In child son  Cons");
		System.out.println(this);
	}
}

class Client{
	public static void main(String [] args){
		 
//		Parent obj1=new Parent();
//		Child obj2=new Child();

	//	obj2.parentprop();
		
		Parent obj=new Childson();         // childson(obj)
		System.out.println(obj);

	}
}
