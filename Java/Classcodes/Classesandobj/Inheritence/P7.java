
class Parent{
	int x=10;
	static int y=20;
	static {
		System.out.println("In parent static block");	
	}
	Parent(){
		System.out.println("In parent Constructor");	

	}
	void Methodone(){
		System.out.println(x);
		System.out.println(y);
	}
	void Methodtwo(){
		System.out.println(x);
		System.out.println(y);
	}
}
class Child extends Parent{
	Child(){	
		System.out.println("In Child Cons");	
	}
	
	static {
		System.out.println("In Child  static block");
	}

}
class Client{

	public static void main(String [] args){
		Child obj=new Child();
		obj.Methodone();
		obj.Methodtwo();
	}
}
