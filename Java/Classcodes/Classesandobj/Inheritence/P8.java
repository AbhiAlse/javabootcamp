class Parent{
	int x=10;
	static int y=20;
	static {
		System.out.println(y);
	}
	Parent(){
		System.out.println("In Parent cons");
	}
}
class child extends Parent{
	int x=100;
	static int y=200;
	child(){
		System.out.println("In child cons");
	}
	void access(){
		System.out.println(super.x);
		System.out.println(super.y);
		System.out.println(x);
		System.out.println(y);
	}
}
class Client{
	public static void main(String [] args){
		child obj=new child();
		obj.access();
	}
}
