/* Static Block parent class
 * static block Child class
 * Constructor of parent class
 * Constructor of child class
 */


class Parent{
	Parent(){

		System.out.println("In parent Cons");	

	}
	static {
		System.out.println("In parent static block");	
	}
}
class Child extends Parent{
	Child(){	
		System.out.println("In Child Cons");	
	}
	
	static {
		System.out.println("In Child  static block");
		
	}

}
class Client{

	public static void main(String [] args){
		Child obj=new Child();
	}
}
