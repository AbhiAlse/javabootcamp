
class Parent{
	static int x=10;
	static {
		System.out.println("In parent static block");	
	}
	static void access(){
		System.out.println(x);
	}
}
class Child extends Parent{
	Child(){	
		System.out.println("In Child Cons");	
	}
	
	static {
		System.out.println("In Child  static block");
		System.out.println(x);
		access();
	}

}
class Client{

	public static void main(String [] args){
		Child obj=new Child();
	}
}
