class Parent{
	int no=10;
	Parent(){
		System.out.println("In cons");
	}
	void inf(){
		System.out.println(no);
	}
}
class Child extends Parent{
	Child(){
		super();    // it will call to parent class
	}
	void Print(){	
		System.out.println(no);
	}
}
class Client{
	public static void main(String [] a){
		Child obj=new Child();
		obj.Print();
		obj.inf();
	}
}


