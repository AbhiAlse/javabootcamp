class Parent{
	int x=10;
	Parent(){
		System.out.println("In Parent cons");
	}
	void access(){
		System.out.println("In Parent instance");
	}
}
class Child extends Parent{
	int y=20;
	Child(){
		System.out.println("In child cons");
		System.out.println(x);
		System.out.println(y);
	}
}
class Client{
	public static void main(String [] args){
		Child obj=new Child();
		obj.access();
	}
}


