class Demo {
	Demo (){
		System.out.println("In cons Demo");
	}
}
class Demochild extends Demo{
	Demochild(){
		System.out.println("In cons demo child");
	}
}
class Parent{
	Parent (){
		System.out.println("In parent Construtor");
	}
	Demo m1(){
		System.out.println("In m1 Parent");
	}
}
class child extends Parent {
	child(){
		System.out.println("In constructor-Child");
	}
	Demo.child.m1(){
		System.out.println("In m1 child");
	}
}
class Client{
	public static void main(String [] args){
		Parent p=new child();
		p.m1();
	}
}
