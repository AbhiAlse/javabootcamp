class Parent {
	int x=10;
	void m1(){
		System.out.println("In parent m1");
	}
}
class child extends Parent {
	int a=20;
	void m1(){
		System.out.println("In child M1");
	}
}
class Demo{
	Demo (Parent p){
		System.out.println("In parent Constructor");
	}
	Demo (child c){
		System.out.println("In Child Constructor");
	}
	public static void main(String [] args){
		Demo obj=new Demo (new Parent());
		Demo obj2=new Demo (new child());
	}
}

