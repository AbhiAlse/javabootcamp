class Outer{
	void m1(){
		System.out.println("In m1 outer");
	}
	static class Inner{
		void m1(){
			System.out.println("In m1 Inner");
		}
	}
	public static void main(String [] args){
		Inner obj=new Outer.Inner();
		obj.m1();
	}

}
/*class Client{
	public static void main(String [] a){
		Outer.Inner obj =new Outer.Inner();
		obj.m1();
	}
}*/

