class Outer{
	Object m1(){
		System.out.println("In m1 Outer");
	
		class Innerone{
			void m1(){
				System.out.println("In m1 Inner");
				
			}
		}
		return new Innerone();
	}
	void m2(){	
		System.out.println("In M2 outer");
	}
}
class Client{
	public static void main(String [] args){
		Outer obj=new Outer();
		//Object obj1=obj.m1();
	//	obj1.m1();//error
		obj.m1().m1();//error = we  cannot return the object 

	}
}
