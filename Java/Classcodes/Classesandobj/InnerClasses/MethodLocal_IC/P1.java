class Outer{
	void m1(){
		System.out.println("In m1 outer");
                 
		//method local Inner class   
		class Inner {
			void m2(){
		   		System.out.println("In m1 Inner"); 
			}
		}
		Inner obj=new Inner();
		obj.m2();

	}
	void m2(){
		System.out.println("In m2 outer");
	}

	public static void main(String [] args){
		Outer obj=new Outer();
		obj.m1();
		obj.m2();
	}
}
