class Outer{
	void m1(){
		System.out.println("In m1 Outer");
	
		class Innerone{
			void m1(){
				System.out.println("In m1 Inner");
				
			}
		}
		Innerone obj=new Innerone ();
		obj.m1();
	}
	void m2(){	
		System.out.println("In M2 outer");
	}
}
class Client{
	public static void main(String [] args){
		Outer obj=new Outer();
		obj.m1();
		obj.m2();
	}
}
