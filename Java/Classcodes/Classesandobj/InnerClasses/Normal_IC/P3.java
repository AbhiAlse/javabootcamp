class Outer{
	class Inner{
		class subInner{
			void m1(){
				System.out.println("In subInner");
			}
		}
	}
}

class Client{
	public static void main(String [] args){
		Outer.Inner.subInner obj=new Outer().new Inner(). new subInner();
		obj.m1();
	}
}
