class Outer {
	int x=10;
	static int y=20;
	class Inner {
		void fun2(){
			//we can make call to var and functions from inner class to outer class 
			System.out.println("In fun2 Inner");
			System.out.println(x);
			System.out.println(y);
			fun();
		}
	}
	void fun(){
		System.out.println("In fun1 Inner");
	}
}
class Client {
	public static void main(String [] args){

	Outer.Inner obj=new Outer().new Inner();
	obj.fun2();

	}
}

