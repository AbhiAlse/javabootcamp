class Outer{
	class Inner{
		void m1(){
			System.out.println("In m1 Inner");
		}
	}
	void m2(){
			System.out.println("In m2 Outer");
	}
}
class Client{
	public static void main(String [] args){
		Outer obj1=new Outer();
		//Outer.Inner obj=new Outer().new Inner();
		Outer.Inner obj2=obj1.new Inner();
		obj2.m1();
		obj1.m2();
	}
}

