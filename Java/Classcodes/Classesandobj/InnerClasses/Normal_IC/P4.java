class Outer {
	class Inner {
		void m1(){
			System.out.println("In m1 Inner");
		}
	}
	void m2(){
		System.out.println("In m2 Outer");
	}

	public static void main(String [] args){
		Inner obj=new Outer().new Inner(); // if main is in same class then no need to give reference = Outer.inner
		obj.m1();
	}
}
