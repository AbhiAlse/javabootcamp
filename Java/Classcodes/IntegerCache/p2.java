class ArrDemo{
	static void fun(int arr[]){

		arr[1]=127;
		arr[2]=128;
	}
	public static void main(String [] args){
		int arr[]={10,20,30,40};
		fun(arr);

		System.out.println(System.identityHashCode(arr[0]));
		System.out.println(System.identityHashCode(arr[1]));
		System.out.println(System.identityHashCode(arr[2]));        //different because out of integercache
		System.out.println(System.identityHashCode(arr[3]));
		
		for(int x:arr){
			System.out.println(x);
		}
		int x=127;
		int y=128;
		System.out.println(System.identityHashCode(x));
		System.out.println(System.identityHashCode(y));              //different 
	}
}

		
