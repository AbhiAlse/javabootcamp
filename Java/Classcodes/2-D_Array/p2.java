class P2{
	public static void main(String [] args){
		int arr[][]=new int [2][2];
		
		arr[0][0]=10;
		arr[0][1]=10;
		arr[1][0]=10;
		arr[1][1]=10;
		System.out.println(arr[1][1]);   //data
		System.out.println(arr[0]);    //adress of 1d array
		System.out.println(arr[1]);     //adress of 1d array  
		System.out.println(arr);       // adress of whole array 
		System.out.println(System.identityHashCode(arr[0][1]));
		System.out.println(System.identityHashCode(arr[1][1]));
		System.out.println(System.identityHashCode(args[0]));      //here the array gets dynamic array
		

	}
}

