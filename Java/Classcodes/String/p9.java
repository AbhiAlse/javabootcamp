class StringDemo{
	public static void main(String []args){
		String str1="Abhishek";
		String str2=new String("Abhishek");
		String str3="Abhishek";
		String str4=new String("Abhishek");

		System.out.println(str1.hashCode());
		System.out.println(str2.hashCode());
		System.out.println(str3.hashCode());
		System.out.println(str4.hashCode());

	}
}
