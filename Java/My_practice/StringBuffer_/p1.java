// user input using BuuferedReader

import java.io.*;
class Demo{
	public static void main(String [] args)throws IOException{ 
	BufferedReader sb=new BufferedReader(new InputStreamReader(System.in));

	System.out.println("Enter the Jer no");
	int Jerno=Integer.parseInt(sb.readLine());

	System.out.println("Enter the age");
	float age=Float.parseFloat(sb.readLine());

	System.out.println("Enter fav character");
	char ch=(char)(sb.read());
	sb.skip(1);         // this is used to catch "\n" it will skip that or else it will be catch by the next variable that is String name

	System.out.println("Enter your name");
	String name=sb.readLine();

	System.out.println("Jer no : "+Jerno);
	System.out.println("Age : "+age);
	System.out.println("Name : "+name);


	}
}
